//REACT LIBRARY
import React, { useEffect, useState } from 'react'; //https://es.reactjs.org/docs/hooks-intro.html
//VTEX LIBRARY
import { useCssHandles } from 'vtex.css-handles'; //https://developers.vtex.com/vtex-developer-docs/docs/vtex-io-documentation-using-css-handles-for-store-customization
import { CSS_HANDLES } from '../../styles/cssHandles';

const Elemento = ({ product, variants, setProduct, currency }) => {
    const [productChange, setProductChange] = useState(product);
    const [show, setShow] = useState(true);
    const handles = useCssHandles(CSS_HANDLES);
    const [firstItem, setfirstItem] = useState(product.id === 1 ? 'hidden' : 'visible');

    useEffect(() => setProductChange(product));

    //ACTUALIZAR PRODUCTO
    const handleChange = () => {
        const randomElement = Math.trunc(Math.random() * variants.products.length);
        //const tallas = [];
        /*
        for (const item of variants.products[randomElement].items) {
            const talla = item.name.split(' ');
            tallas.push(talla[1]);
        }
        */
        setProduct({
            id: productChange.id,
            link: variants.products[randomElement].link,
            itemId: variants.products[randomElement].items[0].itemId,
            nombre: variants.products[randomElement].productName,
            precio: variants.products[randomElement].items[0].sellers[0].commertialOffer.ListPrice,
            options: variants.products[randomElement].items,
            //tallas: tallas,
            //tallaElegida: tallas[0],
            imagen: variants.products[randomElement].items[0].images[0].imageUrl,
            onCheck: true,
        });
    }
    // CAMBIAR LA TALLA
    const handleSize = event => {
        setProduct({
            ...productChange,
            tallaElegida: event.target.value
        });
    }
    // DESACTIVAR O ACTIVAR PRODUCTO
    const handleShow = () => {
        setShow(!show);
        setProduct({
            ...productChange,
            onCheck: !productChange.onCheck,
        });
    }
    //RENDERIZAR JSX
    return (
        <div className={`${handles.pp_item_segundo}`}>
            {
                productChange ? (
                    <div id="elemento-2">
                        <input type="hidden" className="casamarilla-complementa-tu-compra-2-0-x-productIdSuge" value={productChange.itemId} />
                        <a className={`${handles.pp_item_imagen}`} href={productChange.link}>
                            <img className={`${handles.pp_item_imagen_img}`} src={productChange.imagen} />
                        </a>
                        <div className="casamarilla-complementa-tu-compra-2-0-x-pp_item_contenido" id={productChange.itemId}>
                            <a className="casamarilla-complementa-tu-compra-2-0-x-pp_item_nombre" href={productChange.link}>
                                <p className="casamarilla-complementa-tu-compra-2-0-x-pp_item_nombre" >{productChange.nombre}</p>
                            </a>
                            <p className="casamarilla-complementa-tu-compra-2-0-x-pp_item_precio" value={productChange.precio}>{currency(productChange.precio)}</p>
                            
                            <div className={handles.pp_item_actions} >
                                {
                                    show ? (
                                        <>
                                            <div className={handles.pp_contenedor_boton_cambiar} style={{ visibility: firstItem }}>
                                                <button style={{ display: '' }} className={handles.pp_boton_cambiar} type="button" onClick={() => handleChange()}>
                                                    <i class="fas fa-sync"></i>
                                                    <img className={`${handles.pp_item_flechas}`} src="/arquivos/flecha.svg" alt="" crossorigin="anonymous" />
                                                </button>

                                            </div>
                                            <div className={handles.pp_contenedor_boton_eliminar} style={{ visibility: firstItem }}>
                                                <button className={handles.pp_boton_eliminar} type="button" onClick={() => handleShow()} >
                                                    <i></i>
                                                    <img className={`${handles.pp_item_x}`} src="/arquivos/x.svg" alt="" crossorigin="anonymous" />
                                                </button>
                                            </div>
                                        </>
                                    ) : (
                                        <button className={handles.pp_boton_agregar} type="button" onClick={() => handleShow()} >
                                            <i className="fas fa-plus"></i>
                                            Agregar
                                        </button>
                                    )
                                }
                            </div>
                        </div>
                    </div>
                ) : (
                    <div><h1>Loading...</h1></div>
                )
            }
        </div>
    )
}

export default Elemento;