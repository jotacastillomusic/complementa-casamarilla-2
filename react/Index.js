//REACT LIBRARY
import React, { useEffect, useState, useContext } from 'react'; //https://es.reactjs.org/docs/hooks-intro.html
import { injectIntl } from 'react-intl'; //https://formatjs.io/docs/react-intl/api/

//GRAPH QL LIBRARY
import { useQuery } from 'react-apollo'; //https://www.apollographql.com/docs/react/data/queries#usequery-api
import { useLazyQuery } from 'react-apollo'; //https://www.apollographql.com/docs/react/data/queries#manual-execution-with-uselazyquery
import QUERY_ORDER from './types/orderId.gql';
import GET_PRODUCTS from './queries/getProducts.graphql';

//VTEX LIBRARY
import { useRuntime } from 'vtex.render-runtime';  //https://github.com/vtex-apps/render-runtime
import { CSS_HANDLES } from './styles/cssHandles'; //https://developers.vtex.com/vtex-developer-docs/docs/vtex-io-documentation-using-css-handles-for-store-customization
import { useCssHandles } from 'vtex.css-handles';
import { Modal } from 'vtex.styleguide'
import { useProduct } from 'vtex.product-context'

//UTILS
import axios from 'axios'; //https://axios-http.com/docs/intro

//COMPONENTS
import Elemento from './components/Elemento';

//CONTEXT
import ProductContext from './context/ProductContext';

const MyComponent = ({ linkCheckout }) => {
  const [categoria, setCategoria] = useState("1");
  const { productContext: product, first, second, third, setFirst, setSecond, setThird } = useContext(ProductContext);
  const [loadOrder, { data: order }] = useLazyQuery(QUERY_ORDER);
  const [isModalOpen, setIsModalOpen] = useState(false)

  let urlProduct = "";

  const productContextValue = useProduct()

  //VTEX RUNTIME 
  const { getSettings } = useRuntime();
  const handles = useCssHandles(CSS_HANDLES);

  const { data: variants, loading, refetch } = useQuery(GET_PRODUCTS, {
    variables: { categoria },
    ssr: false,
    onCompleted: () => {
      const r1 = Math.trunc(Math.random() * variants.products.length);
      const r2 = Math.trunc(Math.random() * variants.products.length);

      /*
      const tallasSecond = [];
      for (const item of variants.products[r1].items) {
        //console.log(item.variations[0].values[0])
        const talla = item.variations[0].values[0];
        tallasSecond.push(talla);
      }
      */
      setSecond({
        id: 2,
        link: variants.products[r1].link,
        itemId: variants.products[r1].items[0].itemId,
        nombre: variants.products[r1].productName,
        precio: variants.products[r1].items[0].sellers[0].commertialOffer.ListPrice,
        //tallas: tallasSecond,
        //tallaElegida: tallasSecond[0],
        imagen: variants.products[r1].items[0].images[0].imageUrl,
        options: variants.products[r1].items,
        onCheck: true,
      });

      /*
      const tallasThird = [];
      for (const item of variants.products[r2].items) {
        //console.log(item.variations[0].values[0])
        const talla = item.variations[0].values[0];
        tallasThird.push(talla);
      }
      */

      setThird({
        id: 3,
        link: variants.products[r2].link,
        itemId: variants.products[r2].items[0].itemId,
        nombre: variants.products[r2].productName,
        precio: variants.products[r2].items[0].sellers[0].commertialOffer.ListPrice,
        //tallas: tallasThird,
        //tallaElegida: tallasThird[0],
        imagen: variants.products[r2].items[0].images[0].imageUrl,
        options: variants.products[r2].items,
        onCheck: true,
      });
    }
  });

  useEffect(() => {
    loadOrder();

    console.log(productContextValue)

    if (product) {

      if (Object.keys(product).includes('categoryTree')) {
        setCategoria(product.categoryTree[0].id.toString());
        console.log('deberia cambiar a la categoria: ', product.categoryTree)
        refetch();
        if (Object.keys(product).includes('items')) {
          //const tallas = [];
          /*
          for (const item of product.items) {
            const talla = item.name.split(' ');
            tallas.push(talla[1]);
          }
          */
          urlProduct = "/" + product.linkText + "/p";
          setFirst({
            id: 1,
            link: product.link,
            itemId: product.items[0].itemId,
            nombre: product.productName,
            precio: product.priceRange.sellingPrice.highPrice,
            //tallas: tallas,
            //tallaElegida: tallas[0],
            imagen: product.items[0].images[0].imageUrl,
            options: product.items,
            onCheck: true,
          });
        }
      }
    } else {
      setCategoria('1');
    }
  }, [product]);

  if (loading) return null;

  //FORMATEAR INTEGER EN CLP
  const currency = (number) => {
    return new Intl.NumberFormat('es-CL', { style: 'currency', currency: 'CLP', minimumFractionDigits: 0 }).format(number);
  };

  //REALIZAR LA SUMA DE PRODUCTOS
  const Suma = (valor1, valor2, valor3) => {
    let money;
    if (!second.onCheck) {
      money = valor1 + valor3;
    } else if (!third.onCheck) {
      money = valor1 + valor2;
    } else {
      money = valor1 + valor2 + valor3;
    }

    if (!second.onCheck && !third.onCheck) {
      money = valor1;
    }
    return parseInt(money);
  }

  //FUNCIÓN PARA AGREGAR AL CARRITO
  const handleFinish = async () => {
    const orders = [];
    const result1 = first.options.filter(item => {
      const sizeItems = item.name.split(' ');
      if (first.tallaElegida === sizeItems[1]) {
        return item.itemId;
      }
    });
    orders.push({
      quantity: 1,
      seller: '1',
      id: result1[0].itemId,
    });
    if (second.onCheck) {
      const result2 = second.options.filter(item => {
        const sizeItems = item.name.split(' ');
        if (second.tallaElegida === sizeItems[1]) {
          return item.itemId;
        }
      });
      orders.push({
        quantity: 1,
        seller: '1',
        id: result2[0].itemId,
      });
    }

    if (third.onCheck) {
      const result3 = third.options.filter(item => {
        const sizeItems = item.name.split(' ');
        if (third.tallaElegida === sizeItems[1]) {
          return item.itemId;
        }
      });
      orders.push({
        quantity: 1,
        seller: '1',
        id: result3[0].itemId,
      });
    }

    const newOrder = {
      orderItems: orders
    }

    let cadenaFinal = JSON.stringify(newOrder)
    cadenaFinal = cadenaFinal.replace('/', '')
    console.log(cadenaFinal)


    fetch('/api/checkout/pub/orderForm/' + order.orderForm.id + '/items', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: cadenaFinal,
    })
      .then(r => r.json())
      .then(data => handleOpen())
  }

  function handleOpen() {
    setIsModalOpen(true)
  }

  function handleClose() {
    setIsModalOpen(false)
  }


  //RENDERIZAR JSX
  return (
    <div className={`${handles.pp_complementa_tu_compra_container}`}>
      {
        !loading ? (
          <div id="pack_propulsow" className={`${handles.pack_propulsow}`}>
            <div id="pp_div" className={`${handles.pp_div}`}>
              <div className={`${handles.pp_titulo}`}>Complementa tu compra</div>
              <div className={`${handles.pp_elemento}`}>
                <Elemento product={first} variants={variants} setProduct={setFirst} currency={currency} />
                <div className={`${handles.pp_operador}`}>+</div>
                <Elemento product={second} variants={variants} setProduct={setSecond} currency={currency} />
                <div className={`${handles.pp_operador}`}>+</div>
                <Elemento product={third} variants={variants} setProduct={setThird} currency={currency} />
                <div className={`${handles.pp_operador}`}>=</div>

                {/*Total*/}
                <div className={`${handles.pp_total}`}>
                  <div className={`${handles.pp_total_icono}`}>Total Look</div>
                  <p className={`${handles.pp_total_p}`}>
                    3 productos por
                  </p>
                  <span id="suma-final" className={`${handles.total__price}`}>
                    <div id="div-suma">{currency(Suma(first.precio, second.precio, third.precio))}</div>
                  </span>
                  <button
                    onClick={e => handleFinish(e)}
                    className={`${handles.pp_boton_comprar}`}
                    type="button"
                  >
                    Comprar
                  </button>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div> <h1>Cargando...</h1></div>
        )
      }
      <Modal
        isOpen={isModalOpen}
        responsiveFullScreen
        bottomBar={
          <div className={`${handles.nowrap}`}>
            <span className={`${handles.mr4_seguir}`}>
              <a className={`${handles.linkseguir}`} href={urlProduct}>
                Seguir comprando
              </a>
            </span>
            <span className={`${handles.mr4_finalizar}`}>
              <a className={`${handles.linkfinalizar}`} href="/checkout/#/cart">
                Finalizar compra
              </a>
            </span>
          </div>
        }
        onClose={handleClose}>
        <div className={`${handles.texto_modal_kayser}`}>
          <p className={`${handles.p_modal_kayser_enunciado}`}>
            Los productos fueron agregados correctamente a tu compra
          </p>
          <p className={`${handles.p_modal_kayser_pregunta}`}>
            ¿Deseas seguir comprando?
          </p>
        </div>
      </Modal>
    </div>
  )
}

export default injectIntl(MyComponent)