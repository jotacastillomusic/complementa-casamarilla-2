export const CSS_HANDLES = [
    'someHandle1',
    'someHandle2',
    'someHandle3',

    'pack_propulsow',
    'pp_div',
    'pp_titulo',

    'pp_item_primero',
    'pp_elemento',
    'productIdPrimero',
    'productIdSuge',
    'pp_contenedor_sup',
    'pp_item_imagen',
    'pp_item_imagen_img',
    'pp_item_imagen_hidden',
    'pp_imagen',
    'pp_item_imagen',
    'pp_item_contenido_dos',
    'pp_item_contenido',
    'pp_item_nombre',
    'pp_item_precio',

    'pp_operador-01',
    'pp_operador',

    'pp_item_segundo',
    'pp_item_actions',
    'pp_contenedor_boton_cambiar',
    'pp_item_flechas',
    'pp_boton_cambiar',
    'pp_contenedor_boton_eliminar',
    'pp_boton_eliminar',
    'pp_item_x',
    'pp_boton_agregar',

    'pp_total',
    'pp_total_p',
    'pp_total_icono',
    'total__price',
    'pp_boton_comprar',

    'pp_item_tercero',

    'pp_select',
    'pp_contenedor_inferior',
    'pp_option_tallas',
    'pp_item_tallas_text',
    'pp_item_tallas',
    'pp_item_tallas_off',
    'pp_complementa_tu_compra_container',

    'nowrap',
    'mr4_seguir',
    'mr4_finalizar',
    'linkseguir',
    'linkfinalizar',
    'texto_modal_kayser',
    'p_modal_kayser_enunciado',
    'p_modal-kayser_pregunta'

]