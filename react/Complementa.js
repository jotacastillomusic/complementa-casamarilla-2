import React from 'react';
import ProductState from './context/ProductState';
import MyComponent from './Index';

const Complementa = () => {
  return (
    <ProductState>
        <MyComponent />
    </ProductState>
  )
}

export default Complementa;