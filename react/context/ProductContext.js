import { createContext } from 'react'; //https://es.reactjs.org/docs/hooks-intro.html
const productContext = createContext();
export default productContext;