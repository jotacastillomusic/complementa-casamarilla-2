//REACT LIBRARY
import React, { useReducer } from 'react'; //https://es.reactjs.org/docs/hooks-intro.html
//VTEX LIBRARY
import { useProduct } from 'vtex.product-context'; //https://github.com/vtex-apps/product-context
//CONTEXT
import ProductReducer from './ProductReducer';
import ProductContext from './ProductContext';

const ProductState = (props) => {

    const { product } = useProduct();
    const initialState = {
        productContext: {},
        first: {},
        second: {},
        third: {},
    }

    const [state, dispatch] = useReducer(ProductReducer, initialState);

    const setFirst = newProduct => {
        dispatch({
            type: 'SET_PRODUCT_FIRST',
            payload: newProduct
        });
    }

    const setSecond = newProduct => {
        dispatch({
            type: 'SET_PRODUCT_SECOND',
            payload: newProduct
        });
    }

    const setThird = newProduct => {
        dispatch({
            type: 'SET_PRODUCT_THIRD',
            payload: newProduct
        });
    }

    return (
        <ProductContext.Provider value={{
            productContext: product,
            first: state.first,
            second: state.second,
            third: state.third,
            setFirst,
            setSecond,
            setThird,
        }}>
            {props.children}
        </ProductContext.Provider>
    )
}

export default ProductState;