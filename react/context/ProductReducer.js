import { SET_PRODUCT_FIRST, SET_PRODUCT_SECOND, SET_PRODUCT_THIRD } from './types';

export default ( state, action ) => {
    const { payload, type } = action;
    switch (type) {
        case SET_PRODUCT_FIRST:
            return {
                ...state,
                first: payload
            }
        case SET_PRODUCT_SECOND:
            return {
                ...state,
                second: payload
            }
        case SET_PRODUCT_THIRD:
            return {
                ...state,
                third: payload
            }
    
        default:
            return {
                state
            };
    }
}